<?php

namespace Drupal\cloudconvert_media_thumbnail;

use Drupal\media\MediaInterface;

/**
 * Interface MediaThumbnailManagerInterface.
 *
 * @package Drupal\cloudconvert
 */
interface MediaThumbnailManagerInterface {

  /**
   * Validate if the media source field value is changed.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media Entity.
   *
   * @return bool
   *   TRUE if media source has changed.
   */
  public function mediaSourceHasChanged(MediaInterface $media): bool;

}
