<?php

namespace Drupal\cloudconvert_media_thumbnail;

use CloudConvert\Models\Job;
use CloudConvert\Models\Task;
use Drupal\cloudconvert\CloudConvertProcessor;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MediaThumbnailManager.
 *
 * @package Drupal\cloudconvert
 */
class MediaThumbnailManager implements ContainerInjectionInterface, MediaThumbnailManagerInterface {

  /**
   * Cloud Convert Processor.
   *
   * @var \Drupal\cloudconvert\CloudConvertProcessor
   */
  protected CloudConvertProcessor $cloudConvertProcessor;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * Media Type Storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $mediaTypeStorage;

  /**
   * Output format to convert to.
   *
   * @var string
   */
  protected string $outputFormat = 'png';

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * MediaThumbnailManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager.
   * @param \Drupal\cloudconvert\CloudConvertProcessor $cloudConvertProcessor
   *   Cloud Convert Processor.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue Factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, CloudConvertProcessor $cloudConvertProcessor, QueueFactory $queueFactory, ConfigFactoryInterface $configFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->cloudConvertProcessor = $cloudConvertProcessor;
    $this->queueFactory = $queueFactory;
    $this->mediaTypeStorage = $entityTypeManager->getStorage('media_type');
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|\Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('cloudconvert.processor'),
      $container->get('queue'),
      $container->get('config.factory')
    );
  }

  /**
   * Creates a job into cloudconvert to create a thumbnail.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createThumbnailJob(MediaInterface $media) {
    $file = $this->getMediaFile($media);
    $file->save();
    if (!$this->isThumbnailGenerationNeeded($media)) {
      return;
    }

    $cloudConvertTaskTypeStorage = $this->entityTypeManager->getStorage('cloudconvert_task_type');
    /** @var \Drupal\cloudconvert\Entity\CloudConvertTaskTypeInterface $cloudConvertTaskType */
    $cloudConvertTaskType = $cloudConvertTaskTypeStorage->load('media_thumbnail');
    $cloudConvertTask = $this->cloudConvertProcessor->createTask($cloudConvertTaskType, $file);
    $cloudConvertTask->set('field_media', $media);
    $cloudConvert = $this->cloudConvertProcessor->getCloudConvertApi();
    $fileInfo = pathinfo($file->getFileUri());

    $job = (new Job())
      ->addTask(
        (new Task('import/upload', 'import-1'))
      )
      ->addTask(
        (new Task('thumbnail', 'task-1'))
          ->set('input_format', $fileInfo['extension'])
          ->set('output_format', $this->outputFormat)
          ->set('input', ['import-1'])
          ->set('width', 1920)
          ->set('height', 1080)
          ->set('fit', 'max')
          ->set('count', 1)
      )
      ->addTask(
        (new Task('export/url', 'export-1'))
          ->set('input', ['task-1'])
          ->set('inline', FALSE)
          ->set('archive_multiple_files', FALSE)
      );

    $job = $cloudConvert->jobs()->create($job);
    $uploadTask = $job->getTasks()->whereName('import-1')[0];

    $cloudConvert->tasks()->upload($uploadTask, fopen($file->getFileUri(), 'rb'), $file->getFilename());
    $cloudConvertTask->setStep('Waiting on webhook from cloud convert');
    $cloudConvertTask->setProcessId($job->getId());
    $cloudConvertTask->save();
  }

  /**
   * Get the Media File.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media Entity.
   *
   * @return \Drupal\file\FileInterface
   *   File Entity.
   *
   * @throws \InvalidArgumentException
   */
  private function getMediaFile(MediaInterface $media) {
    $mediaType = $this->getMediaType($media);
    $fieldName = $this->getSourceFieldName($mediaType);
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
    $field = $media->get($fieldName);

    /** @var \Drupal\file\FileInterface[] $files */
    $files = $field->referencedEntities();

    return reset($files);
  }

  /**
   * Get the Media Type.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media Entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Media Type Config Entity.
   */
  private function getMediaType(MediaInterface $media) {
    $mediaTypeId = $media->bundle();
    return $this->mediaTypeStorage->load($mediaTypeId);
  }

  /**
   * Get the Source field name.
   *
   * @param \Drupal\media\MediaTypeInterface $mediaType
   *   Media Type Config Entity.
   *
   * @return string
   *   Field Name.
   */
  private function getSourceFieldName(MediaTypeInterface $mediaType) {
    $mediaSource = $mediaType->getSource();
    $fieldDefinition = $mediaSource->getSourceFieldDefinition($mediaType);
    return $fieldDefinition->getName() ?? '';
  }

  /**
   * Validate if thumbnail generation is needed.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media Entity.
   *
   * @return bool
   *   TRUE if thumbnail generation is needed.
   *
   * @throws \InvalidArgumentException
   */
  public function isThumbnailGenerationNeeded(MediaInterface $media): bool {
    $mediaType = $this->getMediaType($media);
    $mediaSource = $mediaType->getSource();
    return $mediaSource->getPluginDefinition()['id'] !== 'image' && !$this->isThumbnailAlreadyCreated($media);
  }

  /**
   * Validate if the thumbnail is already created.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media Entity.
   *
   * @return bool
   *   TRUE if the thumbnail is already created.
   *
   * @throws \InvalidArgumentException
   */
  private function isThumbnailAlreadyCreated(MediaInterface $media): bool {
    $iconsBaseUri = $this->configFactory->get('media.settings')->get('icon_base_uri');
    $defaultIcons = [
      $iconsBaseUri . '/audio.png',
      $iconsBaseUri . '/video.png',
      $iconsBaseUri . '/no-thumbnail.png',
      $iconsBaseUri . '/generic.png',
      $iconsBaseUri . '/video.png',
    ];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $thumbnailField */
    $thumbnailField = $media->get('thumbnail');
    $files = $thumbnailField->referencedEntities();
    if (empty($files)) {
      return FALSE;
    }

    /** @var \Drupal\file\FileInterface $thumbnail */
    $thumbnail = reset($files);
    $fileUri = $thumbnail->getFileUri();

    foreach ($defaultIcons as $iconUri) {
      if ($fileUri === $iconUri) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   */
  public function mediaSourceHasChanged(MediaInterface $media): bool {
    $mediaType = $this->getMediaType($media);
    $fieldName = $this->getSourceFieldName($mediaType);

    if (!isset($media->original)) {
      return FALSE;
    }

    $langcodes = array_keys($media->getTranslationLanguages());
    if ($langcodes === array_keys($media->original->getTranslationLanguages())) {
      return FALSE;
    }

    foreach ($langcodes as $langcode) {
      if (!$this->mediaSourceTranslationHasChanged($media, $fieldName, $langcode)) {
        continue;
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if a translation value of the field is changed.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media Entity.
   * @param string $fieldName
   *   Field Name.
   * @param string $langCode
   *   Language Code.
   *
   * @return bool
   *   TRUE if media source translation has changed.
   *
   * @throws \InvalidArgumentException
   */
  protected function mediaSourceTranslationHasChanged(MediaInterface $media, string $fieldName, string $langCode): bool {
    $items = $media->getTranslation($langCode)
      ->get($fieldName)
      ->filterEmptyItems();
    $original_items = $media->original->getTranslation($langCode)
      ->get($fieldName)
      ->filterEmptyItems();
    // If the field items are not equal, we need to save.
    if ($items->equals($original_items)) {
      return FALSE;
    }

    return TRUE;
  }

}
