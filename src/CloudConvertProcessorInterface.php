<?php

namespace Drupal\cloudconvert;

use CloudConvert\CloudConvert;
use Drupal\cloudconvert\Entity\CloudConvertTaskInterface;
use Drupal\cloudconvert\Entity\CloudConvertTaskTypeInterface;
use Drupal\file\FileInterface;

/**
 * Interface CloudConvertProcessorInterface.
 *
 * @package Drupal\cloudconvert
 */
interface CloudConvertProcessorInterface {

  /**
   * Create a cloud convert task for given file entity.
   *
   * @param \Drupal\cloudconvert\Entity\CloudConvertTaskTypeInterface $cloudConvertTaskType
   *   Cloud Convert Task Type Config Entity.
   * @param \Drupal\file\FileInterface $file
   *   File Entity.
   *
   * @return \Drupal\cloudconvert\Entity\CloudConvertTaskInterface
   *   Cloud Convert Task Entity.
   */
  public function createTask(CloudConvertTaskTypeInterface $cloudConvertTaskType, FileInterface $file);

  /**
   * Download the file and create a file entity.
   *
   * @param \Drupal\cloudconvert\Entity\CloudConvertTaskInterface $cloudConvertTask
   *   Cloud Convert Process.
   *
   * @return string
   *   Download destination.
   */
  public function downloadFile(CloudConvertTaskInterface $cloudConvertTask);

  /**
   * Get the CloudConvert API.
   *
   * @return \CloudConvert\CloudConvert
   *   Cloud Convert API.
   */
  public function getCloudConvertApi(): CloudConvert;

  /**
   * Create a Queue Item to finish a process.
   *
   * @param \Drupal\cloudconvert\Entity\CloudConvertTaskInterface $cloudConvertTask
   *   Cloud Convert Task Entity.
   */
  public function createFinishQueueItem(CloudConvertTaskInterface $cloudConvertTask);

}
