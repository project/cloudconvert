<?php

namespace Drupal\cloudconvert;

use CloudConvert\CloudConvert;
use Drupal\cloudconvert\Entity\CloudConvertTaskInterface;
use Drupal\cloudconvert\Entity\CloudConvertTaskTypeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\file\FileInterface;
use GuzzleHttp\Psr7\Utils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CloudConvertProcessor.
 *
 * @package Drupal\cloudconvert
 */
class CloudConvertProcessor implements ContainerInjectionInterface, CloudConvertProcessorInterface {

  /**
   * Cloud Convert API key.
   *
   * @var string
   */
  protected string $apiKey;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * CloudConvert API.
   *
   * @var \CloudConvert\CloudConvert\Cloudconvert
   */
  protected CloudConvert $cloudConvertApi;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystem;

  /**
   * Construct a CloudConvertProcessor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   QueueFactory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactory $configFactory, QueueFactory $queueFactory, FileSystemInterface $fileSystem) {
    $cloudConvertSettings = $configFactory->get('cloudconvert.settings');
    $apiKey = $cloudConvertSettings->get('api_key');
    $sandBox = $cloudConvertSettings->get('sandbox');
    $this->cloudConvertApi = new CloudConvert([
      'api_key' => $apiKey,
      'sandbox' => $sandBox,
    ]);

    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->queueFactory = $queueFactory;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('queue'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCloudConvertApi(): CloudConvert {
    return $this->cloudConvertApi;
  }

  /**
   * {@inheritdoc}
   */
  public function createTask(CloudConvertTaskTypeInterface $cloudConvertTaskType, FileInterface $file) {
    $cloudConvertTaskStorage = $this->entityTypeManager->getStorage('cloudconvert_task');

    return $cloudConvertTaskStorage->create([
      'original_file_id' => $file->id(),
      'type' => $cloudConvertTaskType->id(),
      'process_info' => [],
      'process_parameters' => [],
      'step' => 'to-do',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function downloadFile(CloudConvertTaskInterface $cloudConvertTask): string {
    $file = $cloudConvertTask->get('process_parameters')->getValue()[0]['result']['files'][0];
    $replace = FileSystemInterface::EXISTS_RENAME;
    $baseDestination = 'temporary://';
    $destinationUri = $this->fileSystem->getDestinationFilename($baseDestination . $file['filename'], $replace);

    $this->downloadFileToDrupal($destinationUri, $file['url']);

    return $destinationUri;
  }

  /**
   * Downloads a file to the drupal system.
   *
   * @param string $fileDestination
   *   Destination of the file.
   * @param string $outputUrl
   *   The file itself.
   */
  public function downloadFileToDrupal(string $fileDestination, string $outputUrl): void {
    $local = Utils::streamFor(fopen($fileDestination, 'wb'));
    $local->write(file_get_contents($outputUrl));
  }

  /**
   * Create a Queue Item.
   *
   * @param \Drupal\cloudconvert\Entity\CloudConvertTaskInterface $cloudConvertTask
   *   Cloud Convert Task Entity.
   * @param string $queueName
   *   Queue.
   * @param array $parameters
   *   List of parameters.
   */
  private function createQueueItem(CloudConvertTaskInterface $cloudConvertTask, $queueName, array $parameters = []) {
    /** @var \Drupal\Core\Queue\QueueInterface $queueName */
    $queue = $this->queueFactory->get($queueName);
    $item = new \stdClass();
    $item->cloudconvert_task_id = $cloudConvertTask->id();
    $queue->createItem($item);
  }

  /**
   * {@inheritdoc}
   */
  public function createFinishQueueItem(CloudConvertTaskInterface $cloudConvertTask) {
    $this->createQueueItem($cloudConvertTask, 'cloudconvert_finish_processor');
  }

}
