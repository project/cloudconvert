<?php

namespace Drupal\cloudconvert\Exceptions;

use Exception;

/**
 * InvalidParameterException exception is thrown.
 *
 * When a request failed because of a bad client configuration.
 *
 * InvalidParameterException appears when the request failed because of a bad
 * parameter from the client request.
 *
 * @package CloudConvert
 * @category Exceptions
 */
class InvalidParameterException extends Exception {

}
