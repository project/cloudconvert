<?php

namespace Drupal\cloudconvert\Exceptions;

use Exception;

/**
 * ApiException exception is thrown.
 *
 * When a the CloudConvert API returns any HTTP error code.
 *
 * @package CloudConvert
 * @category Exceptions
 */
class ApiException extends Exception {

}
