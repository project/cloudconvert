<?php

namespace Drupal\cloudconvert\Exceptions;

/**
 * ApiBadRequestException exception is thrown when a the CloudConvert API.
 *
 * Returns any HTTP error code 400.
 *
 * @package CloudConvert
 * @category Exceptions
 */
class ApiBadRequestException extends ApiException {

}
