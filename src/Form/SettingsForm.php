<?php

namespace Drupal\cloudconvert\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\cloudconvert\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudconvert_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cloudconvert.settings');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#size' => 2048,
      '#maxlength' => 2048,
      '#required' => TRUE,
    ];

    $form['sandbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Connect to the sandbox API'),
      '#default_value' => $config->get('sandbox'),
    ];

    $form['webhook_signature'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook signature'),
      '#description' => $this->t('Make a webhook <a href="https://cloudconvert.com/dashboard/api/v2/webhooks">here and paste the signing signature of the webhook here'),
      '#default_value' => $config->get('webhook_signature'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cloudConvertSettings = $this->configFactory->getEditable('cloudconvert.settings');

    $cloudConvertSettings->set('api_key', $form_state->getValue('api_key'));
    $value = !empty($form_state->getValue('sandbox'));
    $cloudConvertSettings->set('sandbox', $value);
    $cloudConvertSettings->set('webhook_signature', $form_state->getValue('webhook_signature'));

    $cloudConvertSettings->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudconvert.settings',
    ];
  }

}
