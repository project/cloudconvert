<?php

namespace Drupal\cloudconvert\Controller;

use Drupal\cloudconvert\CloudConvertProcessorInterface;
use Drupal\cloudconvert\Event\CloudConvertErrorEvent;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\Utils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use function GuzzleHttp\json_decode;

/**
 * Class WebHookController.
 *
 * @package Drupal\cloudconvert\Controller
 */
class WebHookController extends ControllerBase {

  /**
   * Current Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $currentRequest;

  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * Cloud Convert Processor.
   *
   * @var \Drupal\cloudconvert\CloudConvertProcessorInterface
   */
  protected CloudConvertProcessorInterface $cloudConvertProcessor;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private EventDispatcherInterface $eventDispatcher;

  /**
   * Construct the webhook controller.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityType manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   * @param \Drupal\cloudconvert\CloudConvertProcessorInterface $cloudConvertProcessor
   *   Cloud convert processor.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RequestStack $requestStack, QueueFactory $queueFactory, CloudConvertProcessorInterface $cloudConvertProcessor, EventDispatcherInterface $eventDispatcher) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->queueFactory = $queueFactory;
    $this->cloudConvertProcessor = $cloudConvertProcessor;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('queue'),
      $container->get('cloudconvert.processor'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Callback to handle a cloudconvert finished request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request done on the webhook endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON Response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\InvalidArgumentException
   */
  public function webhook(Request $request): JsonResponse {
    $taskData = Utils::jsonDecode($request->getContent(), TRUE);
    if ($taskData['event'] === 'job.created') {
      return new JsonResponse();
    }

    $cloudConvertTasks = $this->entityTypeManager->getStorage('cloudconvert_task')->loadByProperties(
      ['process_id' => $taskData['job']['id']]
    );

    /** @var \Drupal\cloudconvert\Entity\CloudConvertTask $cloudConvertTask */
    $cloudConvertTask = reset($cloudConvertTasks);
    if ($taskData['event'] === 'job.failed') {
      $cloudConvertTask->setStep('Finished');
      $cloudConvertTask->save();
      $event = new CloudConvertErrorEvent($cloudConvertTask, $taskData);
      $this->eventDispatcher->dispatch($event, CloudConvertErrorEvent::ERROR);
      return new JsonResponse();
    }

    $this->cloudConvertProcessor->createFinishQueueItem($cloudConvertTask);
    $cloudConvertTask->setStep('queued for downloading');
    $tasks = $taskData['job']['tasks'];

    $downloadTasks = array_filter($tasks, static function ($task) {
      return $task['operation'] === 'export/url';
    });

    $cloudConvertTask->setProcessParameters($downloadTasks);
    $cloudConvertTask->save();

    return new JsonResponse();
  }

  /**
   * Access validation.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Contains a account entity.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access Result Allowed or Forbidden.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\InvalidArgumentException
   */
  public function access(AccountInterface $account) {
    $request = $this->currentRequest;
    if (!$this->validRequestSignature($request)) {
      return AccessResult::forbidden('Webhook signature mismatch.');
    }

    $taskData = json_decode($request->getContent(), TRUE);
    $cloudConvertTasks = $this->entityTypeManager->getStorage('cloudconvert_task')->loadByProperties(
      ['process_id' => $taskData['job']['id']]
    );

    $amountOfTasks = count($cloudConvertTasks);
    if (count($cloudConvertTasks) > 1) {
      return AccessResult::forbidden('There are multiple entries for one job.');
    }

    if (empty($amountOfTasks)) {
      return AccessResult::forbidden('There is no entry for the job.');
    }

    $cloudConvertTask = reset($cloudConvertTasks);

    if ($cloudConvertTask->getStep() === 'finished') {
      return AccessResult::forbidden('CloudConvert Task is already marked as finished.');
    }

    return AccessResult::allowed();
  }

  private function validRequestSignature(?Request $request) {
    return true;

    //@todo: Fix code below.
    $webhookSignature = $this->config('cloudconvert.settings')->get('webhook_signature');
    $requestSignature = $request->headers->get('HTTP_CLOUDCONVERT_SIGNATURE');
    $signature = hash_hmac('sha256', $request->getContent(), $webhookSignature);
    return $requestSignature === $signature;
  }
}
