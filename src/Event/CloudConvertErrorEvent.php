<?php

namespace Drupal\cloudconvert\Event;

/**
 * Class CloudConvertFinishEvent.
 *
 * @package Drupal\cloudconvert\Event
 */
class CloudConvertErrorEvent extends CloudConvertBaseEvent {

  public const ERROR = 'cloudconvert.error';

}
