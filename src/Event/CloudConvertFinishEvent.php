<?php

namespace Drupal\cloudconvert\Event;

/**
 * Class CloudConvertFinishEvent.
 *
 * @package Drupal\cloudconvert\Event
 */
class CloudConvertFinishEvent extends CloudConvertBaseEvent {

  public const FINISH = 'cloudconvert.finish';

}
