<?php

namespace Drupal\cloudconvert\Plugin\EventSubscriber;

use Drupal\cloudconvert\Event\CloudConvertErrorEvent;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * CloudConvertErrorSubscriber file.
 */
class CloudConvertErrorSubscriber implements EventSubscriberInterface {

  /**
   * The logger channel to log to.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $loggerChanner;

  /**
   * CloudConvertErrorSubscriber constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The default logger channel.
   */
  public function __construct(LoggerChannelInterface $loggerChannel) {
    $this->loggerChanner = $loggerChannel;
  }

  /**
   * {@inheritDoc}.
   */
  public static function getSubscribedEvents() {
    return [CloudConvertErrorEvent::ERROR => 'onCloudConvertError'];
  }

  /**
   * Acts on a cloudconvert failed job.
   *
   * @param \Drupal\cloudconvert\Event\CloudConvertErrorEvent $event
   *   The error event.
   */
  public function onCloudConvertError(CloudConvertErrorEvent $event): void {
    $task = $event->getCloudConvertTask();
    $result = $event->getResult();

    $this->loggerChanner->error(
      sprintf(
        'Task %s has an error processing on cloudconvert on id : %s. More info %s',
        $task->id(),
        $task->getProcessInfo(),
        print_r($result, TRUE)
      )
    );
  }

}
