<?php

/**
 * @file
 * Contains cloudconvert_task.page.inc.
 *
 * Page callback for CloudConvert Task entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for CloudConvert Task templates.
 *
 * Default template: cloudconvert_task.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cloudconvert_task(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}
